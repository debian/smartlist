#define VERSION "\
 v3.15 2000/08/25\n\
    Copyright (c) 1990-1999, Stephen R. van den Berg\t<srb@cuci.nl>\n\
    Copyright (c) 1999-2000, Philip A. Guenther\t\t<guenther@gac.edu>\n\
\n\
Submit questions/answers to the procmail-related mailinglist by sending to:\n\
\t<procmail-users@procmail.org>\n\
\n\
And of course, subscription and information requests for this list to:\n\
\t<procmail-users-request@procmail.org>\n"

/* If the formatting or number of newlines of VERSION substantially changes,
   src/autoconf and src/manconf.c need to be changed as well.  And yes,
   there is supposed to be a leading space */
