smartlist (3.15-25) unstable; urgency=medium

  * debian/fakeroot-workaround: Move to debian/rules.
  * debian/rules: Drop obsolete comments and set permissions in a more
    straightforward way. Hopefully the package is reproducible now.
  * debian/rules: Add recommended targets build-arch and build-indep.
  * debian/rules: Use dpkg-buildflags.

 -- Santiago Vila <sanvila@debian.org>  Sun, 30 Aug 2015 16:33:14 +0200

smartlist (3.15-24) unstable; urgency=low

  * Use gzip -n to stop recording current time. Closes: #777445.
  * Fix mtimes before building binary package as well.

 -- Santiago Vila <sanvila@debian.org>  Fri, 15 May 2015 16:45:16 +0200

smartlist (3.15-23) unstable; urgency=medium

  * Switch to 3.0 (quilt) source format. The source for this package
    was the sum of upstream procmail 3.15 and smartlist 3.15 tarballs.
    To make things simpler, this package used to contain all the patches
    that were also applied to the procmail package, even if not needed.
    To differentiate easily between them I'm using "procmail-" and
    "smartlist-" as prefix in debian/patches.
  * Remove "rc.init" and "reject" at /var/list/.etc on purge, so that
    no traces are left. Discovered by piuparts. Closes: #670263.
  * Changed rc.request to allow larger messages to be sent to the -request
    address. Raised threshold for "sane" size from 4096 to 16384 bytes.
    Reported by Thomas Preud'homme. Closes: #700216.
  * Removed way obsolete Recommends on base-passwd.
  * Removed way obsolete Conflicts on suidmanager.
  * Fixed lintian overrides syntax.

 -- Santiago Vila <sanvila@debian.org>  Sat, 03 May 2014 21:37:36 +0200

smartlist (3.15-22) unstable; urgency=low

  * Fixed several scripts to deal with domain names starting with numbers.
    Thanks to Cord Beermann. Closes: #580703.

 -- Santiago Vila <sanvila@debian.org>  Tue, 24 Aug 2010 01:34:14 +0200

smartlist (3.15-21) unstable; urgency=low

  * Changes in preinst:
  - Removed obsolete code which removes buggy postrm when upgrading from
    a release older than 3.10-12 (nearly 13 years ago).
  - Removed obsolete warning about hardlinks in .etc when upgrading
    from a release older than 3.10.7-2 (more than 10 years ago).
  - Trust that base-passwd does its job and do not ask the user to press
    Enter if user list does not exist, it has wrong uid or gid, or it has
    a wrong home directory. Exit code is still 1 if such thing ever happens.
  - Removed backwards compatibility code which creates a backup of rc.init
    when upgrading from a release older than 3.10-13 (nearly 13 years ago).
  * Changes in postinst:
  - Removed obsolete code which creates rc.init from backup copy.
  - Removed obsolete code which updates rc.init when upgrading from a
    release older than 3.15-2 (8 years ago).
  - Removed obsolete code which creates symlinks for confirm-error.txt
    and rc.rfc822 for lists created before 3.15-6 (7 years ago).
  - Added code which adds listid variable to already existing rc.init.
  * Changes in postrm:
  - On purge, remove aliases from /etc/aliases unconditionally.
    A backup copy is created in either case.
  - On purge, do not remove /var/list, as it may contain user data.
  * The above changes remove all user interaction. Closes: #553295, #566678.
  * Fixed mail-transport-agent dependency. Closes: #495838.
  * Added List-Id header in rc.init and rc.submit.
  * Modified digest script to add List-* headers to digests as well.
    Patch by Cord Beermann. Thanks a lot. Closes: #457415.
  * Added lintian overrides for files that may not be moved to /etc easily.

 -- Santiago Vila <sanvila@debian.org>  Mon, 25 Jan 2010 12:32:36 +0100

smartlist (3.15-20) unstable; urgency=high

  * Make autoconf.h without cflags, then make everything else as usual,
    as we did in procmail. Fixes FTBFS problem on s390.

 -- Santiago Vila <sanvila@debian.org>  Thu, 18 Jan 2007 19:24:12 +0100

smartlist (3.15-19) unstable; urgency=medium

  * Updated README.exim for exim4. Renamed to README.exim4. Closes: #375554.
  * Updated QuickStart so that it refers to README.exim4.
  * Do not chown var/list/.procmailrc in debian/fakeroot-workaround, as it
    is a symlink and we don't want to change the owner of the pointed file.

 -- Santiago Vila <sanvila@debian.org>  Thu,  4 Jan 2007 15:39:06 +0100

smartlist (3.15-18) unstable; urgency=medium

  * Upgraded to confirm-1.3.5. This should fix a subtle bug in which
    email addresses starting with `_' were not handled properly.
    For reference, this is CAN-2005-0157.

 -- Santiago Vila <sanvila@debian.org>  Mon, 14 Feb 2005 18:42:46 +0100

smartlist (3.15-17) unstable; urgency=low

  * While we are at it, modified postinst so that an empty reject file
    is created in the first install if it does not exist, instead of
    waiting for the first invocation of `createlist'.

 -- Santiago Vila <sanvila@debian.org>  Mon, 22 Nov 2004 16:11:40 +0100

smartlist (3.15-16) unstable; urgency=low

  * Added the procmail -> /usr/bin/procmail symlink to the bin directory.
    This should avoid surprises when a list is "created" by restoring the
    list directory from a backup instead of using the `createlist' script.
    This was first reported by Evgeny Stambulchik as Bug #201693.

 -- Santiago Vila <sanvila@debian.org>  Sun, 21 Nov 2004 16:01:54 +0100

smartlist (3.15-15) unstable; urgency=low

  * Fixed spelling of "subcription" to "subscription" everywhere.
  * Changing the domain variable in rc.custom does not work unless
    listaddr, listreq, listdist and sendmailOPT are redefined as well.
    Added commented lines to rc.custom for this (Closes: #233931).
    This should eliminate the need to delink rc.init.

 -- Santiago Vila <sanvila@debian.org>  Tue, 10 Aug 2004 17:01:44 +0200

smartlist (3.15-14) unstable; urgency=low

  * Upgraded to confirm-1.3.4.
  - An exact match is required to determine whether or not a new subscriber
    is already subscribed (Closes: #70848).
  - Addresses with regexp meta-characters (like +) should now be able to
    unsubscribe (Closes: #215342).
  - The bug which was fixed in 3.15-11 is now fixed in a slightly
    different way (the way Werner fixed it in confirm-1.3.3).
  * Reformatted extended description (Closes: #211951).

 -- Santiago Vila <sanvila@debian.org>  Fri, 24 Oct 2003 18:37:38 +0200

smartlist (3.15-13) unstable; urgency=low

  * Changed "From: $listreq" to "From: SmartList <$listreq>" to lower the
    SpamAssassin score of messages generated by SmartList (Closes: #178353)
  * Make sure /usr/share/doc/smartlist/* have sane permissions even
    when built using an odd umask (Closes: #204318).

 -- Santiago Vila <sanvila@debian.org>  Sat,  9 Aug 2003 19:40:30 +0200

smartlist (3.15-12) unstable; urgency=low

  * Fixed a subtle bug in rc.request which made certain email addresses
    unable to subscribe. Patch by Werner Reisberger (Closes: #188686).

 -- Santiago Vila <sanvila@debian.org>  Tue, 15 Apr 2003 16:39:30 +0200

smartlist (3.15-11) unstable; urgency=low

  * Addresses containing `+' should now really be able to subscribe.

 -- Santiago Vila <sanvila@debian.org>  Thu, 13 Feb 2003 17:39:24 +0100

smartlist (3.15-10) unstable; urgency=low

  * Use a random boundary for multipart/digest messages (Closes: #132405).
    This will take effect once the current digest is flushed, to not break
    existing digests.

 -- Santiago Vila <sanvila@debian.org>  Sun,  9 Feb 2003 13:36:06 +0100

smartlist (3.15-9) unstable; urgency=low

  * Upgraded to confirm-1.3.1. There was a bug in rc.request which made
    subscriptions not to work at all (Closes: #171931).

 -- Santiago Vila <sanvila@debian.org>  Sun,  8 Dec 2002 19:12:40 +0100

smartlist (3.15-8) unstable; urgency=low

  * Upgraded the confirm suite to version 1.3.0.
  * Use /etc/mailname as the default value for the `domain' variable
    in rc.init (Closes: #169765).

 -- Santiago Vila <sanvila@debian.org>  Thu, 28 Nov 2002 17:54:38 +0100

smartlist (3.15-7) unstable; urgency=low

  * Upgraded the confirm suite to version 1.2.9. This should make
    impossible for an user to unsubscribe another one when using cookies.
  * README.exim: Added a recipe (adapted from the exim FAQ) to forget
    about /etc/aliases when creating new mailing lists.

 -- Santiago Vila <sanvila@debian.org>  Sat,  9 Nov 2002 17:47:54 +0100

smartlist (3.15-6) unstable; urgency=low

  * Upgraded the confirm suite to version 1.2.6. This should make most
    RFC822-compliant addresses to be accepted (Closes: #151195).
  - Deprecated `bin' file: confirm, but it will not be removed yet.
  - New `bin' files: confirm_add, confirm_del and extraddr.
  - New `etc' files: rc.rfc822 and confirm-error.txt.
  - Changed `etc' files: rc.confirm, rc.request and confirm.txt.
  - Updated createlist so that it creates symlinks to new `etc' files.
  - Modified postinst so that it creates new required symlinks for
    already existing lists.
  * Removed archive.txt, help.txt, mimencap.local, subscribe.txt
    and unsubscribe.txt from the examples directory, since they are
    already in the `bin' or `etc' directories.
  * Removed README from the binary package, since it does not contain
    any useful info not already in the copyright file.
  * Removed Manifest and INSTALL as well, since they are misleading.
  * Simplified QuickStart and README.confirm.
  * Updated copyright file.
  * Enhanced README.exim.

 -- Santiago Vila <sanvila@debian.org>  Fri, 18 Oct 2002 22:00:50 +0200

smartlist (3.15-5) unstable; urgency=low

  * Added support for DEB_BUILD_OPTIONS.
  * Improved spelling of extended description (Closes: #125364).
  * Changed Manual so that it explains the moderator_PASSWORD variable.
    Note: The rc.custom file already contained some hints about it.
  * Changed QuickStart as well.

 -- Santiago Vila <sanvila@debian.org>  Sun, 20 Jan 2002 16:32:02 +0100

smartlist (3.15-4) unstable; urgency=high

  * Restored multigram setuid bit. Was lost in 3.15-3 (Closes: #113048).
    Breaks sendmail in this way: flist: Can't find ".etc" in "/usr/sbin".
    Thanks a lot to Andrea Gelmini for the report.

 -- Santiago Vila <sanvila@debian.org>  Sun, 23 Sep 2001 13:36:50 +0200

smartlist (3.15-3) unstable; urgency=medium

  * SmartList/install.sh now hardcodes /usr/sbin/sendmail as the location
    of the `sendmail' binary, instead of using procmail to determine that,
    which failed under woody when using fakeroot (Closes: #106763).
  * Make sure files have the right permissions even when using fakeroot.
  * Removed build-dependency on procmail.

 -- Santiago Vila <sanvila@debian.org>  Fri, 27 Jul 2001 14:32:12 +0200

smartlist (3.15-2) unstable; urgency=low

  * postinst updates rc.init and adds new variables when upgrading
    from older releases. Not doing so breaks list archiving.
  * Removed suidmanager support and Conflicts: suidmanager (<< 0.50).

 -- Santiago Vila <sanvila@debian.org>  Fri, 13 Jul 2001 20:20:52 +0200

smartlist (3.15-1) unstable; urgency=low

  * New upstream release.
  * Updated copyright file.
  * Build-Depends: procmail.
  * Closes: Bug#59769: Send submitted messages to maintainer if moderators
    file is needed but missing.
  * Closes: Bug#61443. Added a small paragraph in QuickStart explaining that
    the Debian package uses symlinks instead of hardlinks and the rationale.
  * Closes: Bug#65112, reported by Davide G. M. Salvetti <salve@hal.linux.it>.
  - Automatic cross-posting of messages sent to a digested list over to the
    undigested list only worked if the latter contained the letter 'y'.
  - When forwarding a message from a digest list to its corresponding
    undigested list, remove any Delivered-To: header field containing the name
    of the digested list, thus allowing the message forwarded back to go out.

 -- Santiago Vila <sanvila@debian.org>  Sun, 14 Jan 2001 20:02:35 +0100

smartlist (3.13-3) unstable; urgency=low

  * Fixed a typo in confirm-help.txt (Closes: Bug#61445).
  * Standards-Version: 3.1.1.

 -- Santiago Vila <sanvila@debian.org>  Mon, 14 Aug 2000 13:41:26 +0200

smartlist (3.13-2) unstable; urgency=low

  * Removed set -x from `led' (Bug #49463).

 -- Santiago Vila <sanvila@ctv.es>  Wed, 10 Nov 1999 11:54:44 +0100

smartlist (3.13-1) unstable; urgency=low

  * New upstream release.
  * Upgraded to confirm-1.2.3. This makes confirm.*[su][0-9]+ messages
    to be filtered out of the list and diverted (Bug #25542).
  * Fixed Y2K problem in digest processing (Bug #33579). Volume numbers
    for years >= 2000 will be the year itself (not the year minus 1900).
    For backwards compatibility, current volume numbers (like `99') will
    remain unchanged.
  * rc.request: rewrite free email ads (Bugs #34197 and #36065).

 -- Santiago Vila <sanvila@ctv.es>  Mon, 19 Apr 1999 14:13:08 +0200

smartlist (3.10.7-9) unstable; urgency=low

  * Updated the confirmation suite by Werner Reisberger.

 -- Santiago Vila <sanvila@ctv.es>  Tue, 30 Mar 1999 18:19:19 +0200

smartlist (3.10.7-8) unstable; urgency=low

  * removelist: Fixed wrong reference to /usr/lib/aliases (Bug #30415).

 -- Santiago Vila <sanvila@ctv.es>  Fri,  5 Mar 1999 17:43:39 +0100

smartlist (3.10.7-7) frozen unstable; urgency=low

  * donatelist: Reverse the order for chown and chmod (Bug #27253).
  * Added README.exim, regarding "Resent-To" (Bug #27150).

 -- Santiago Vila <sanvila@ctv.es>  Tue, 24 Nov 1998 17:35:13 +0100

smartlist (3.10.7-6) unstable; urgency=low

  * Slightly improved confirm-help.txt (Bug #25582).
  * Added a comment about "cc:Mail" in the doc directory.
  * rc.custom and rc.init: default minbounce raised to 10.
  * rc.request: Updated according to the author's version.
  * rc.request: Some Novell GroupWise messages are now directed to /dev/null.

 -- Santiago Vila <sanvila@ctv.es>  Sun, 30 Aug 1998 18:04:52 +0200

smartlist (3.10.7-5) frozen unstable; urgency=medium

  * Added the confirmation suite by Werner Reisberger (fixes Bug #13661).
  * New conffiles in .etc: rc.confirm, confirm.txt and confirm-help.txt.
  * Updated .bin/createlist so that it adds symlinks to those files.
  * Added a README.confirm explaining how it works.
  * Added an important warning about hardlinks created by older
    releases of .bin/createlist.

 -- Santiago Vila <sanvila@ctv.es>  Fri, 29 May 1998 17:47:56 +0200

smartlist (3.10.7-4) frozen unstable; urgency=medium

  * Improved rc.request to catch an unusual queue warning (Bug #22091).
  * Fixed multigram.c to reduce load (patch by the author).
  * Standards-Version: 2.4.1.

 -- Santiago Vila <sanvila@ctv.es>  Wed, 13 May 1998 22:19:32 +0200

smartlist (3.10.7-3) unstable; urgency=low

  * Pristine-ish source (only -ish, since the tarball is made
    from both procmail and SmartList sources).
  * Fixed Bug #17612: "Press any key" --> "Press Enter".
  * Removed debstd dependency.

 -- Santiago Vila <sanvila@ctv.es>  Sun,  8 Feb 1998 20:13:08 +0100

smartlist (3.10.7-2) unstable; urgency=low

  * Patched .bin/createlist to use symlinks wherever possible.
  * QuickStart: Added a small note about "trusted users", removed the "exec"
    word in aliases file and removed all references to <listname>-dist.
  * Added mimencap.local as a conffile.

 -- Santiago Vila <sanvila@ctv.es>  Thu, 23 Oct 1997 19:11:57 +0200

smartlist (3.10.7-1) unstable; urgency=low

  * Upgraded to 3.11pre7. Sources are now GPLed, hurrah!
  * Added explicit SEARCHLIBS, to avoid unneeded dependency on libdl.
  * Patched .bin/createlist to avoid the use of |exec in /etc/aliases,
    which does not work with some MTAs.
  * First libc6 release.

 -- Santiago Vila <sanvila@ctv.es>  Wed, 18 Jun 1997 21:15:43 +0200

smartlist (3.10-16) frozen unstable; urgency=low

  * Rebuilt using latest debmake to avoid a problem with suidmanager.

 -- Santiago Vila <sanvila@ctv.es>  Sat, 17 May 1997 20:52:59 +0200

smartlist (3.10-15) frozen unstable; urgency=low

  * Source changes for allowing an already running smartlist to work while
    a new smartlist package is being built.

 -- Santiago Vila <sanvila@ctv.es>  Sat, 3 May 1997 12:42:15 +0200

smartlist (3.10-14) unstable; urgency=low

  * Rewritten control file extended description.
  * Added a small note about rc.custom and X_COMMAND_PASSWORD in QuickStart.
  * debian/rules build target now just "make setid", since procmail has
    not to be built.
  * Rewritten copyright file.
  * initmake unmodified (source).

 -- Santiago Vila <sanvila@ctv.es>  Sun, 23 Mar 1997 12:04:34 +0100

smartlist (3.10-13) unstable; urgency=low

  * Moved user `list' checking from postinst to preinst. Decreased
    verbosity. Now it is quiet if everything is ok.
  * Removed .etc/rc.init.dist from conffiles.
  * Removed .etc/rc.init from the package, is is created from
    .etc/rc.init.dist (not from itself) or restored from a backup file
    (which is created by preinst if older version < 3.10-13).
  * Recommends: base-passwd 1.3.0.
  * Changed "ListMaster" alias by "listman". Of course, this will take
    effect only in new installs.

 -- Santiago Vila <sanvila@ctv.es>  Sun, 16 Mar 1997 18:12:38 +0100

smartlist (3.10-12) unstable; urgency=medium

  * Simplified postinst. No more prompting for "announce" sample list.
  * Added QuickStart in /usr/doc/smartlist.
  * preinst removes old buggy postrm if old version < 3.10-12.
  * list aliases in /etc/aliases are only removed by postrm if purge.
  * postrm makes a backup copy of /etc/aliases just in case.
  * Removed /var/list/.procmailrc from conffiles and replaced by a symlink.
  * Added a small note in debian/rules regarding a problem which
    may arise when recompiling this package.

 -- Santiago Vila <sanvila@ctv.es>  Mon, 24 Feb 1997 19:15:33 +0100

smartlist (3.10-11) unstable; urgency=low

  * Added conffiles.
  * Some minor debian/rules changes.
  * Removed fix-substvars script, since it's no longer needed with
    new libc5_5.4.20.
  * Recommends: base-passwd >= 2.0.0-0. Hope it will exists soon :-)
  * Added MD5 sums.

 -- Santiago Vila <sanvila@ctv.es>  Sat, 22 Feb 1997 16:25:32 +0100

smartlist (3.10-10) unstable; urgency=low

  * Now list home directory should be /var/list.
  * Changed base-passwd dependency by just a Recommends, allowing the
    user to create `list' user by hand if he/she wishes. Reasonable checks
    are made to verify that the proper list user does exist. Advise to
    upgrade base-passwd if this does not happen.
  * Modified debian/rules binary target, now to install it uses a
    symlink /var/list -> debian/tmp/var/list.
  * purge does only rm -rf /var/list when the user agrees after
    being asked.

 -- Santiago Vila <sanvila@ctv.es>  Sun, 12 Jan 1997 01:00:54 +0100

smartlist (3.10-9) unstable; urgency=low

  * New cleaned up .orig source from original FTP site.
  * debian/rules build target may now be invoked without root privileges.
  * Moved examples (one of them is automagically generated) from
    /var/list/.examples to /usr/doc/smartlist/examples.
  * Added the symlink changelog.gz -> HISTORY.gz.
  * Moved generated `Manual' from /var/list/.etc to /usr/doc/smartlist.
  * Added `fix-substvars' script to depend on libc5 >= 5.4.0.
  * Put CFLAGS settings &c in ./Makefile to be inherited by src/Makefile.
  * New maintainer.

 -- Santiago Vila <sanvila@ctv.es>  Thu, 26 Dec 1996 02:23:22 +0100

smartlist (3.10-8) stable unstable; urgency=low

  * Made dependant on base-passwd instead of base
  * fixed postinst dependance on homedirectory of user list
  * fixed postrm removal of group/userid

 -- Christoph Lameter <clameter@debian.org>  Tue, 17 Dec 1996 05:28:41 -0800

smartlist (3.10-7) frozen unstable; urgency=low

  * Wrong dependency should have depended on base 1.1.0-? and not base 1.2.x

 -- Christoph Lameter <clameter@debian.org>  Wed, 11 Dec 1996 15:07:00 -0800

smartlist (3.10-6) frozen unstable; urgency=low

  * Suidmanager support
  * Static UID/GID

 -- Christoph Lameter <clameter@debian.org>  Thu, 21 Nov 1996 12:46:19 -0800

smartlist (3.10-5) unstable; urgency=low

  * Fixed newalias call in postrm

 -- Christoph Lameter <clameter@debian.org>  Fri, 1 Nov 1996 11:07:49 -0800

smartlist (3.10-4) unstable; urgency=low

  * Made postinst not fail on newaliases (smail problem)

 -- Christoph Lameter <clameter@debian.org>  Fri, 1 Nov 1996 07:00:41 -0800

smartlist (3.10-3) unstable; urgency=low

  * bug in usage of hostname in postinst
  * debstd : Compressed doc manpages, file locations revised
  * Outstanding still: Static uid/gid

 -- Christoph Lameter <clameter@debian.org>  Tue, 22 Oct 1996 19:15:10 -0700

smartlist (3.10-2) unstable; urgency=low

  * Upgrade to standards 2.1.0.0
  * Fixed bug in postinst script with hostname 2.0
  * Questions user if a mailing list should be generated.
  * Outstanding issue: Assignment of static uid/gid (No answer from Bruce)

 -- Christoph Lameter <clameter@debian.org>  Sun, 08 Sep 1996 15:37:25 +0100
